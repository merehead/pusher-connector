<?php

namespace MereHead\PusherConnector\Helpers\Facades;


use Illuminate\Support\Facades\Facade;

class Pusher extends Facade {


    protected static function getFacadeAccessor()
    {
        return 'pusher';
    }

}