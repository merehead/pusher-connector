<?php

namespace MereHead\PusherConnector;

use MereHead\PusherConnector\Services\PusherService;
use Illuminate\Support\ServiceProvider;

class PusherServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \App::bind('pusher', function () {
            return new PusherService();
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $config = __DIR__ . '/Config/config.php';

        $this->publishes([
            $config => config_path('pusherconnector.php'),
        ], 'config');

        $this->mergeConfigFrom($config, 'pusherconnector');
    }

}
