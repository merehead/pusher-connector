<?php

namespace MereHead\PusherConnector\Services;

use Pusher;


class PusherService
{
    private $chanelName = null;
    private $pushData   = null;
    private $tcp        = null;
    private $port       = null;
    private $context    = null;
    private $socket    = null;

    function __construct()
    {
        $this->tcp     = config('pusherconnector.PUSHER_TCP');
        $this->port    = config('pusherconnector.PUSHER_PORT');
        $this->context = new \ZMQContext();

        //Get Socket
        $this->socket = $this->context->getSocket(\ZMQ::SOCKET_PUSH, null);
        //Connect port
        $this->socket->connect("tcp://".$this->tcp.":".$this->port);

    }

    public function push($chanelName, $data)
    {
        $this->chanelName = $chanelName;
        $this->pushData   = $data;

        $validation = $this->validatePush();

        if($validation['validate']){
            return response()->json($this->sendData());
        }
        return response()->json($validation);
    }


    private function validatePush()
    {
        $validate = ['validate' => true, 'error_message' => []];

        if(!is_string($this->chanelName)){
            $validate['validate'] = false;
            $validate['error_message'][] = 'Chanel_name  must bu string';
        }

        if(!is_array($this->pushData)){
            $validate['validate'] = false;
            $validate['error_message'][] = 'Send data must be array';
        }

        if(!$this->tcp){
            $validate['validate'] = false;
            $validate['error_message'][] = 'PUSHER_TCP not found in env file';
        }

        if(!$this->port){
            $validate['validate'] = false;
            $validate['error_message'][] = 'PUSHER_PORT not found in env file';
        }

        return $validate;
    }

    private function sendData()
    {
        //Send data
        $this->socket->send(json_encode(['chanel_name' => $this->chanelName,'data' => $this->pushData]));
        return true;
    }



}