# merehead

#Installation
Require this package in Laravel project composer.json and run composer update
        
        require {
            .....
            .....
            
            "merehead/pusher-connector": "dev-master"
            
            .....
            .....
        }
        
After updating composer, add the service provider line at the begining of providers array in /config/app.php

    'providers' => [
            .........................................
            .........................................
            
            MereHead\PusherConnector\PusherServiceProvider::class
            
            ...................
            ........................
      ]
      'aliases' => [
      
            .....................
            .....................
            
            'Pusher' => MereHead\PusherConnector\Helpers\Facades\Pusher::class,
      
            ......................
            ........................
      ]
# Edit config
 
If you want to edit config you need to run

    php artisan vendor:publish --provider="merehead/pusher-connector" --tag=config 

    php artisan vendor:publish

So config-file will be moved to /config/pusherconnector.php and can be edited as you want and changes will not be lost after composer update.